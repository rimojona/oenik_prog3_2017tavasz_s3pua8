﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Game
{
    public abstract class Element
    {
        protected int x, y;
        protected double rot;
        protected Geometry alak;

        public Geometry Alak
        {
            get
            {
                TransformGroup tg = new TransformGroup();
                tg.Children.Add(new TranslateTransform(x, y));
                tg.Children.Add(new RotateTransform(rot, x, y));
                alak.Transform = tg;
                return alak;
            }
            set
            {
                alak = value;
            }
        }
    }

    public abstract class Auto : Element
    {
        public Auto()
        {
            RectangleGeometry r1 = new RectangleGeometry(new Rect(-20, -20, 40, 30));
            RectangleGeometry r2 = new RectangleGeometry(new Rect(-20, -36, 4, 12));
            RectangleGeometry r3 = new RectangleGeometry(new Rect(-16, -40, 32, 20));
            RectangleGeometry r4 = new RectangleGeometry(new Rect(16, -36, 4, 12));
            RectangleGeometry r5 = new RectangleGeometry(new Rect(-20, 14, 4, 12));
            RectangleGeometry r6 = new RectangleGeometry(new Rect(-16, 10, 32, 20));
            RectangleGeometry r7 = new RectangleGeometry(new Rect(16, 14, 4, 12));
            Geometry c1 = Geometry.Combine(r1, Geometry.Combine(r2, Geometry.Combine(r3, r4, GeometryCombineMode.Union, null), GeometryCombineMode.Union, null), GeometryCombineMode.Union, null);
            Geometry c2 = Geometry.Combine(r5, Geometry.Combine(r6, r7, GeometryCombineMode.Union, null), GeometryCombineMode.Union, null);
            Alak = Geometry.Combine(c1, c2, GeometryCombineMode.Union, null);
        }

    }
}
