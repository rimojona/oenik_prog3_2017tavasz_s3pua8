﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Game
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
    }

    

    public class Palya : FrameworkElement
    {
        public event EventHandler JatekVege;
        DispatcherTimer dt;
        JatekosAuto jatekos;
        List<EllensegesAuto> ellensegek;
        int elet = 3;
        int pont = 0;

        public Palya()
        {
            
            jatekos = new JatekosAuto();
            Loaded += Palya_Loaded;
        }

        private void Palya_Loaded(object sender, RoutedEventArgs e)
        {
            ellensegek = new List<EllensegesAuto>();
            //int res = (int)((ActualHeight - 100) / 3);
            for (int i = 0; i < 4; i++)
            {
                EllensegesAuto a = new EllensegesAuto(ActualWidth, ActualHeight, i * (-200));
                a.JarAPont += A_JarAPont;
                ellensegek.Add(a);
            }
            dt = new DispatcherTimer() { Interval = TimeSpan.FromMilliseconds(5) };
            dt.Tick += Dt_Tick;
            dt.Start();
            (this.Parent as Window).KeyDown += Palya_KeyDown;
        }

        private void A_JarAPont(object sender, EventArgs e)
        {
            pont++;
        }

        private void Palya_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
                jatekos.Mozog(-10, (int)ActualWidth);
            else if (e.Key == Key.Right)
                jatekos.Mozog(10, (int)ActualWidth);
            InvalidateVisual();

        }

        private void Dt_Tick(object sender, EventArgs e)
        {
            foreach (EllensegesAuto e1 in ellensegek)
            {
                e1.Mozog(ActualHeight);
            }
            bool mostutkozik = false;
            foreach (EllensegesAuto ell in ellensegek)
                if (jatekos.UtkozikEVele(ell))
                {
                    if (!jatekos.Utkozik)
                    {
                        elet--;
                        if (elet < 0)
                        {
                            if (JatekVege != null)
                                JatekVege(this, EventArgs.Empty);
                            dt.Stop();
                        }
                    }
                    mostutkozik = true;
                }
            jatekos.Utkozik = mostutkozik;
            InvalidateVisual();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            drawingContext.DrawGeometry(Brushes.LightGray, new Pen(Brushes.Black, 1), jatekos.Alak);
            if(ellensegek != null)
            {
                foreach (EllensegesAuto e1 in ellensegek)
                {
                    drawingContext.DrawGeometry(Brushes.Pink, new Pen(Brushes.Black, 1), e1.Alak);
                }
            }
            FormattedText ft = new FormattedText(
                "Élet: " + elet + ", pont: " + pont,
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Arial"),
                12,
                Brushes.Black);
            drawingContext.DrawGeometry(Brushes.Black, null, ft.BuildGeometry(new Point(10, 10)));
        }
    }
}
