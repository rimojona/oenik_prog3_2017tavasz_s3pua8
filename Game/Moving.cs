﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Game
{
    public class JatekosAuto : Auto
    {
        bool utkozik;

        public bool Utkozik
        {
            get
            {
                return utkozik;
            }

            set
            {
                utkozik = value;
            }
        }

        public JatekosAuto() : base()
        {
            x = 150;
            y = 400;
        }

        public void Mozog(int dx, int aw)
        {
            x += dx;
            if (x <= 20)
            {
                x = 20;
            }
            else if (x >= aw - 20)
            {
                x = aw - 20;
            }
        }

        public bool UtkozikEVele(Element e)
        {
            return Geometry.Combine(this.Alak.GetFlattenedPathGeometry(), e.Alak.GetFlattenedPathGeometry(), GeometryCombineMode.Intersect, null).GetArea() > 0;
        }
    }

    public class EllensegesAuto : Auto
    {
        public event EventHandler JarAPont;
        double aw, ah;
        static Random R = new Random();

        public EllensegesAuto(double aw, double ah, int y) : base()
        {
            this.aw = aw;
            this.ah = ah;
            this.y = y;
            AutoInit();
        }
        

        void AutoInit()
        {
            x = R.Next(40, (int)aw - 40);
        }

        public void Mozog(double ah)
        {
            if(y - 40 > ah)
            {
                y = -20;
                AutoInit();
                if (JarAPont != null)
                    JarAPont(this, EventArgs.Empty);
            }
            y++;
        }
    }
}
